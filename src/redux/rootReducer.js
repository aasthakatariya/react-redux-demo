import {combineReducers} from 'redux'

import {default as cakeReducer} from  './cake/cakeReducer'
import {default as iceCreamReducer} from  './iceCream/iceCreamReducer'
import {default as userReducer} from './user/userReducer'

const rootReducer= combineReducers({
    cake:cakeReducer,
    iceCream:iceCreamReducer,
    user:userReducer
})

export default rootReducer