import React from 'react';
import logo from './logo.svg';
import './App.css';
import CakeContainer from './components/CakeContainer';
import IceCreamContainer from './components/IceCreamContainer';
import NewCakeContainer from './components/NewCakeContainer';
import ItemContainer from './components/ItemContainer';

import HooksCakeContainer from './components/HooksCakeContainer';

import {Provider} from 'react-redux'
import store from './redux/store'
import UserContainer from './components/UserContainer';

function App() {
  return (
    <Provider store={store}>
    <div className="App">
    <UserContainer/>
    {/* <ItemContainer cake/>
    <ItemContainer/>
    <HooksCakeContainer/>
     <CakeContainer/>
     <IceCreamContainer/>
     <NewCakeContainer/> */}

    </div>
    </Provider>
  );
}

export default App;
